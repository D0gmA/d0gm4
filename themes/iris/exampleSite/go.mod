module github.com/peaceiris/hugo-theme-iris/exampleSite

go 1.15

replace github.com/peaceiris/hugo-theme-iris => ../

require (
	github.com/hakimel/reveal.js v0.0.0-20210827074853-01d8d669bc2b // indirect
	github.com/jaywcjlove/hotkeys v3.8.7+incompatible // indirect
	github.com/jgthms/bulma v0.0.0-20210828105430-67ab02b2890d // indirect
	github.com/mathjax/MathJax v0.0.0-20210617144258-7146ffa47956 // indirect
	github.com/mermaid-js/mermaid v8.8.5-0.20210826174357-48b6a32fe19a+incompatible // indirect
	github.com/peaceiris/hugo-mod-bulma v0.3.0 // indirect
	github.com/peaceiris/hugo-mod-mathjax v0.2.0 // indirect
	github.com/peaceiris/hugo-mod-mermaidjs v0.9.0 // indirect
	github.com/peaceiris/hugo-mod-revealjs v0.4.0 // indirect
	github.com/peaceiris/hugo-theme-iris v0.41.1 // indirect
)
